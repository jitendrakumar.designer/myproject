import React from "react";
import ReactDOM from "react-dom";
// import App from "./ReactRouter/App";
import App from "./Website/App";
import { BrowserRouter, Route, Switch } from "react-router-dom";

ReactDOM.render(
  <>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </>,
  document.getElementById("root")
);
